import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

@IonicPage({
  segment: 'stats/:key',
  defaultHistory: ['ListPage']
})
@Component({
  selector: 'page-stats',
  templateUrl: 'stats.html',
})
export class StatsPage {

  stats: any;
  constructor(private api: ApiProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {

    const key = this.navParams.get('key')

    if (!key) {
      return this.navCtrl.setRoot('ListPage')
    }
    
    this.getURLStats(key)
  }


  getURLStats(key) {

    this.api.get('url/stats/' + key, {}).subscribe(res => {

      if (res.success) {
        this.stats = res.data
      }

    }, err => {

    })

  }
}
