import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  urlForm: FormGroup;
  url: string = ''

  showUrl = false
  shortUrl: any

  errMessage: string = ""

  constructor(private api: ApiProvider, private formBuilder: FormBuilder) {

    this.urlForm = this.formBuilder.group({
      url: ['', Validators.compose([Validators.required, Validators.minLength(3)])]
    });
  }

  ionViewDidLoad() {

    this.url = ""
    this.showUrl = false
    this.shortUrl = null
    this.errMessage = ""

  }

  generate() {

    this.showUrl = false
    this.shortUrl = null

    if (!this.url || !this.url.trim()) {
      this.showUrl = true
      this.errMessage = 'please enter a valid URL'
      return;
    }

    this.api.post('url/generate', { url: this.url }).subscribe(res => {

      if (res.success) {

        this.shortUrl = res.data

      } else {

        this.errMessage = 'unable to process this url'
      }

      this.showUrl = true

    }, err => {

      this.errMessage = err.message
      this.showUrl = false

    })

  }

}
