import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {

  urls: any[] = []
  constructor(private api: ApiProvider, public navCtrl: NavController) {
  }

  ionViewDidLoad() {

    this.getUrls()

  }

  getUrls() {

    this.api.get('url/all', {}).subscribe(res => {

      if (res.success) {
        this.urls = res.data
      }

    }, err => {

    })
  }


  getUrlStats(url) {

    this.navCtrl.push('StatsPage', { key: url.shortKey })

  }
}
