import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

export interface _Response {
  success: Boolean,
  data: any,
  user: any,
  token: any
}

@Injectable()
export class ApiProvider {

  public host: String = 'http://localhost:4555'

  constructor(public http: HttpClient) { }

  post(url: string, data: any) {
    return this.http.post(`${this.host}/api/${url}`, data).map((data) => data as _Response)
  }

  get(url: string, params: any) {

    let _params = new HttpParams()

    if (params) {

      for (const key in params) {
        _params = _params.set(key, params[key])
      }
    }

    return this.http.get(`${this.host}/api/${url}`, { params: _params }).map((data) => data as _Response)
  }

  delete(url: string, params: any) {

    let _params = new HttpParams()

    if (params) {

      for (const key in params) {
        _params.set(key, params[key])
      }
    }


    return this.http.delete(`${this.host}/api/${url}`, { params: _params }).map((data) => data as _Response)
  }

  put(url: string, data: any) {

    return this.http.put(`${this.host}/api/${url}`, data).map((data) => data as _Response)
  }


}
